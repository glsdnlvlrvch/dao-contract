import { task } from "hardhat/config";


task("vote", "Vote for proposal with particular id")
    .addParam("id", "id of proposal")
    .addParam("flag", "vote flag(true or false)")
    .addParam("dao", "contract of DAO")
    .addParam("account", "public key of your account")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("DAO", taskArgs.dao);
        const voteFlag = taskArgs.flag == "true" ? true : false;

        await contract.connect(taskArgs.account).vote(
            taskArgs.id,
            voteFlag
        );
    });