import { task } from "hardhat/config";


task("finish", "Finish particular proposal")
    .addParam("id", "id of proposal")
    .addParam("dao", "contract of DAO")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("DAO", taskArgs.dao);
        await contract.finishProposal(
            taskArgs.id,
        );
    });