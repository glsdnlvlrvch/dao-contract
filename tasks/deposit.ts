import { task } from "hardhat/config";


task("deposit", "Deposit tokens in DAO")
    .addParam("amount", "amount of tokens")
    .addParam("dao", "contract of DAO")
    .addParam("account", "public key of your account")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("DAO", taskArgs.dao);
        await contract.connect(taskArgs.account).deposit(
            taskArgs.amount
        );
    });