import { task } from "hardhat/config";


task("add", "Add proposal for voting")
    .addParam("callData", "callData for calling function")
    .addParam("recipient", "address of contract for calling func")
    .addParam("description", "description of proposal")
    .addParam("chairperson", "chair person account address")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("DAO", taskArgs.dao);
        await contract.connect(taskArgs.chairperson).addProposal(
            taskArgs.callData,
            taskArgs.recipient,
            taskArgs.description
        );
    });