# :space_invader: DAO (Voting)
Basic implementation of decentralized autonomous organization, especially voting functionality</br>

***ETHERSCAN***: https://rinkeby.etherscan.io/address/0xD911b00e068cA1D8E6787A89Be887159A0d3308A#code </br>


## Tasks
- add (add proposal for voting)
- finish (finish proposal)
- deposit (deposit tokens for voting)
- vote (vote for particular proposal)
