//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract DAO is AccessControl {
    bytes32 public constant chairPerson = keccak256("chairPerson");
    address public voteToken;
    uint256 public minimumQuorum;
    uint256 public debatingPeriodDuration;
    uint256 public percentToAccept; // percent to accept proposal
    uint256 public proposalIds;

    struct Voter {
        uint256 deposit;
        uint256[] proposalsFor;
        uint256[] proposalsAgainst;
    }

    struct Proposal {
        uint256 startTime;
        bytes callData;
        address recipient;
        string description;
        uint256 votesFor;
        uint256 votesAgainst;
        uint256 quorum;
    }

    mapping(address => Voter) private _voters;
    mapping(uint256 => Proposal) private _proposals;

    enum ProposalResult { ACCEPTED, DECLINED }

    event AddProposal(
        uint256 indexed id,
        address indexed recipient,
        string indexed description,
        bytes callData
    );
    event FinishProposal(
        uint256 indexed id,
        address indexed recipient,
        string indexed description,
        ProposalResult result
    );

    constructor(
        address _chairPerson, 
        address _voteToken,
        uint256 _minimumQuorum,
        uint256 _debatingPeriodDuration,
        uint256 _percentToAccept
    ) {
        _setupRole(chairPerson, _chairPerson);

        voteToken = _voteToken;
        minimumQuorum = _minimumQuorum;
        debatingPeriodDuration = _debatingPeriodDuration;
        percentToAccept = _percentToAccept;
    }
    
    function deposit(uint256 amount) external {
        IERC20(voteToken).transferFrom(
            msg.sender, 
            address(this), 
            amount
        );

        Voter storage v = _voters[msg.sender];
        v.deposit += amount;
    }

    function addProposal(
        bytes memory callData,
        address recipient,
        string memory description
    ) external onlyRole(chairPerson) {
        Proposal storage p = _proposals[proposalIds];
        p.callData = callData;
        p.recipient = recipient;
        p.description = description;
        p.startTime = block.timestamp;

        emit AddProposal(
            proposalIds, 
            p.recipient, 
            p.description, 
            p.callData
        );

        proposalIds++;
    }

    function vote(
        uint256 id, 
        bool supportAgainst
    ) external {
        require(
            _proposals[id].startTime != 0,
            "invalid id"
        );
        Proposal storage p = _proposals[id];
        Voter storage v = _voters[msg.sender];
        
        if(supportAgainst) {
            p.votesFor += 1;
            v.proposalsFor.push(id);
        }
        else {
            p.votesAgainst += 1;
            v.proposalsAgainst.push(id);
        }

        p.quorum += v.deposit;
    }

    function finishProposal(
        uint256 id
    ) external {
        Proposal storage p = _proposals[id];
        require(
            p.startTime != 0,
            "invalid proposal"
        );
        require(
            p.quorum >= minimumQuorum,
            "not enough quorum"
        );
        require(
            block.timestamp - p.startTime > debatingPeriodDuration,
            "still voting"
        );

        ProposalResult result = ProposalResult.DECLINED;

        if(votesResult(p)){
            address(p.recipient).call(p.callData);
            result = ProposalResult.ACCEPTED;
        }

        emit FinishProposal(
            id, 
            p.recipient, 
            p.description,
            result
        );

        delete _proposals[id];

    }

    function withdraw(
        uint256 amount
    ) external {
        Voter storage v = _voters[msg.sender];
        require(
            v.deposit >= amount,
            "not enough deposit"
        );
        require(
            !isVoting(msg.sender),
            "sender in voting"
        );

        IERC20(voteToken).transfer( 
            msg.sender,
            amount
        );

        v.deposit -= amount;
    }

    function votesResult(
        Proposal memory prop
    ) internal view returns(bool) {
        return prop.votesFor * 100 / (prop.votesFor + prop.votesAgainst) >= percentToAccept;
    }

    function isVoting(
        address user
    ) internal view returns(bool) {
        Voter storage v = _voters[user];

        for(uint i = 0; i < v.proposalsFor.length; i++){
            uint256 id = v.proposalsFor[i];
            if(_proposals[id].startTime != 0) return true;
        }

        for(uint i = 0; i < v.proposalsAgainst.length; i++){
            uint256 id = v.proposalsAgainst[i];
            if(_proposals[id].startTime != 0) return true;
        }

        return false;
    }

    function changeVoteToken(
        address newVoteToken
    ) external onlyRole(chairPerson) {
        voteToken = newVoteToken;
    }

    function addChairPerson(
        address newChairPerson
    ) external onlyRole(chairPerson) {
        _grantRole(
            chairPerson, 
            newChairPerson
        );
    }

    function removeChairPerson(
        address oldChairPerson
    ) external onlyRole(chairPerson) {
        _revokeRole(
            chairPerson, 
            oldChairPerson
        );
    }

    function changeMinimumQuorum(
        uint256 newQuorum
    ) external {
        require(
            msg.sender == address(this),
            "only with proposal"
        );

        minimumQuorum = newQuorum;
    }

    function changePercentsToAccept(
        uint256 newPercents
    ) external {
        require(
            msg.sender == address(this),
            "only with proposal"
        );

        percentToAccept = newPercents;
    }
}
