import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";
import { keccak256 } from "ethers/lib/utils";
import { exitCode } from "process";

describe("DAO", function () {
    let signers: Signer[];
    let contract: Contract;

    let chairPerson: string;
    let voteToken: Contract;
    const minimumQuorum: number = 300;
    const debatingPeriodDuration: number = 15;
    const percentToAccept: number = 51;

    beforeEach(async function () {
        signers = await ethers.getSigners();
        const owner = signers[0];
        chairPerson = await owner.getAddress();

        let TokenFactory = await ethers.getContractFactory("Token");
        voteToken = await TokenFactory.deploy(
            "KZ Token",
            "KZT"
        );
        
        let DAOFactory = await ethers.getContractFactory("DAO");
        contract = await DAOFactory.deploy(
            chairPerson,
            voteToken.address,
            minimumQuorum,
            debatingPeriodDuration,
            percentToAccept
        );
        await contract.deployed();
        
        for (var i = 0; i < 5; i++){
            await voteToken.connect(signers[0]).mint(
                await signers[i].getAddress(),
                100
            );
            await voteToken.connect(signers[i]).approve(
                contract.address,
                100
            )
        }
    });

    it("addProposal", async () => {
        const callData = voteToken.interface.encodeFunctionData("test", [
            "200"
        ])
        const recipient = await signers[1].getAddress();
        const description = "some desc";

        await expect(
            contract.connect(signers[1]).addProposal(   // signer[0] is chairPerson
                callData,
                recipient,
                description
            )
        ).to.be.revertedWith(
            "AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x2d204d6f0bcad8aeeebdce97598e7dc83c325b9b5bd26ecb915f2e606de4ae78"
        );

        const tx = contract.addProposal(
            callData,
            recipient,
            description
        )

        await expect(
            tx
        ).to.emit(
            contract,
            "AddProposal"
        ).withArgs(
            0,
            recipient,
            description,
            callData
        );
    })

    it("deposit && vote", async () => {
        let user1 = await signers[1].getAddress();
        let user2 = await signers[2].getAddress();

        const callData = voteToken.interface.encodeFunctionData("test", [
            "200"
        ])
        const recipient = await signers[1].getAddress();
        const description = "some desc";

        contract.addProposal(
            callData,
            recipient,
            description
        )

        expect(
            await voteToken.balanceOf(user1)
        ).to.be.eq(100);
        expect(
            await voteToken.balanceOf(user2)
        ).to.be.eq(100);


        await contract.connect(signers[1]).deposit(100);
        await contract.connect(signers[2]).deposit(100);

        expect(
            await voteToken.balanceOf(user1)
        ).to.be.eq(0);
        expect(
            await voteToken.balanceOf(user2)
        ).to.be.eq(0);

        await expect(
            contract.vote(33, true)
        ).to.be.revertedWith("invalid id");
    })

    it("finishProposal && withdraw", async () => {
        await expect(
            contract.finishProposal(0)
        ).to.be.revertedWith("invalid proposal");

        const callData1 = voteToken.interface.encodeFunctionData("test", [
            "200"
        ])
        const recipient1 = voteToken.address;
        const description1 = "some desc";
        const callData2 = voteToken.interface.encodeFunctionData("test", [
            "300"
        ])
        const recipient2 = voteToken.address;
        const description2 = "some desc";

        await contract.addProposal(
            callData1,
            recipient1,
            description1
        );

        await expect(
            contract.finishProposal(0)
        ).to.be.revertedWith("not enough quorum");

        await contract.addProposal(
            callData2,
            recipient2,
            description2
        );

        for (var i = 1; i < 5; i++) {
            await contract.connect(signers[i]).deposit(100);
            await contract.connect(signers[i]).vote(0, true);
            await contract.connect(signers[i]).vote(1, false);
        }

        await expect(
            contract.finishProposal(0)
        ).to.be.revertedWith("still voting");

        await ethers.provider.send("evm_increaseTime", [15000]);

        const tx1 = contract.finishProposal(0);

        await expect(
            contract.connect(signers[1]).withdraw(20)
        ).to.be.revertedWith("sender in voting");

        await contract.addProposal(
            callData2,
            recipient2,
            description2
        );
        await contract.connect(signers[1]).vote(2, true);
        await ethers.provider.send("evm_increaseTime", [15000]);

        await expect(
            contract.connect(signers[1]).withdraw(20)
        ).to.be.revertedWith("sender in voting");

        const tx2 = contract.finishProposal(1);

        await expect(tx1)
            .to.emit(contract, "FinishProposal")
            .withArgs(
                0,
                recipient1,
                description1,
                0
            );

        await expect(tx2)
            .to.emit(contract, "FinishProposal")
            .withArgs(
                1,
                recipient2,
                description2,
                1
            );

        expect(
            await voteToken.value()
        ).to.be.eq(200);
        
        await expect(
            contract.connect(signers[6]).withdraw(10)
        ).to.be.revertedWith("not enough deposit");

        await contract.connect(signers[2]).withdraw(20);    // signers[0]
        expect(
            await voteToken.balanceOf(
                await signers[2].getAddress()
            )
        ).to.be.eq(20);
    });

    it("admin functions", async () => {
        expect(
            await contract.voteToken()
        ).to.not.be.eq(contract.address);

        contract.changeVoteToken(
            contract.address
        );

        expect(
            await contract.voteToken()
        ).to.be.eq(contract.address);

        const callData = voteToken.interface.encodeFunctionData("test", [
            "200"
        ])
        const recipient = voteToken.address;
        const description = "some desc";

        await expect(
            contract.connect(signers[1]).addProposal(
                callData,
                recipient,
                description
            )
        ).to.be.reverted;

        await contract.addChairPerson(
            await signers[1].getAddress()
        );

        await expect(
            contract.connect(signers[1]).addProposal(
                callData,
                recipient,
                description
            )
        ).to.not.be.reverted;

        await contract.removeChairPerson(
            await signers[1].getAddress()
        );

        await expect(
            contract.connect(signers[1]).addProposal(
                callData,
                recipient,
                description
            )
        ).to.be.reverted;
    })

    it("voting to change percent", async () => {
        const callData = contract.interface.encodeFunctionData("changePercentsToAccept", [
            "57"
        ])
        const recipient = contract.address;
        const description = "We should change percent to accept a Proposal to 57% for...";

        expect(
            await contract.percentToAccept()
        ).to.be.eq(51);

        await contract.addProposal(
            callData,
            recipient,
            description
        );
        
        for (var i = 0; i < 5; i++) {
            await contract.connect(signers[i]).deposit(100);
            await contract.connect(signers[i]).vote(0, true);
        }
        await ethers.provider.send("evm_increaseTime", [15000]);

        await contract.finishProposal(0);

        expect(
            await contract.percentToAccept()
        ).to.be.eq(57);

        await expect(
            contract.changePercentsToAccept(1)
        ).to.be.revertedWith("only with proposal");
    })

    it("voting to change quorum", async () => {
        const callData = contract.interface.encodeFunctionData("changeMinimumQuorum", [
            "200"
        ])
        const recipient = contract.address;
        const description = "We should change percent to accept a Proposal to 57% for...";

        expect(
            await contract.minimumQuorum()
        ).to.be.eq(300);

        await contract.addProposal(
            callData,
            recipient,
            description
        );
        
        for (var i = 0; i < 5; i++) {
            await contract.connect(signers[i]).deposit(100);
            await contract.connect(signers[i]).vote(0, true);
        }
        await ethers.provider.send("evm_increaseTime", [15000]);

        await contract.finishProposal(0);

        expect(
            await contract.minimumQuorum()
        ).to.be.eq(200);

        await expect(
            contract.changeMinimumQuorum(1)
        ).to.be.revertedWith("only with proposal");
    });
});
