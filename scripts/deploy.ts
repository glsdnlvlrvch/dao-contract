import { ethers } from "hardhat";
import * as dotenv from "dotenv";
dotenv.config();

async function main() {
  const chairPerson = process.env.CHAIR_PERSON_ADDR || "";
  const tokenAddr = process.env.TOKEN_ADDR || "";
  const minQuorum = process.env.MIN_QUORUM || "";
  const percent2Access = process.env.PERCENT || "";
  const debatinPeriod = process.env.DEBATING_TIME || "";
  
  const Factory = await ethers.getContractFactory("DAO");
  const dao = await Factory.deploy(
    chairPerson,
    tokenAddr,
    minQuorum,
    debatinPeriod,
    percent2Access
  );
  await dao.deployed();

  console.log("DAO deployed to:", dao.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
